import axios, {
  baseURL
} from 'httpConfig';

export const login = (params) => axios('/user/login',params).post();
